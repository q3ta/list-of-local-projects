<?php
$sites = array_diff(scandir("/var/www/"), [".", "..", "html"]);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>List of websites</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" type="image/x-icon" href="img/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <div class="wrap">
      <section class="time js-time">
        <span class="time__item js-time-hours"></span>
        <span class="time__item js-time-minutes"></span>
        <span class="time__item js-time-seconds"></span>
      </section>
      <section class="search">
        <input class="search__input js-search-site" type="text" placeholder="site.loc...">
      </section>
      <ul class="list">
        <?php foreach($sites as $site): ?>
          <li class="list__item js-list-item" data-url="<?=$site; ?>">
            <a class="list__link" href="http://<?=$site; ?>" target="_blank"><?=$site; ?></a>
          </li>
        <?php endforeach; ?>
        <li class="list__item js-list-item" data-url="PhpMyAdmin">
          <a class="list__link list__link--develop" href="http://localhost/phpmyadmin" target="_blank">PhpMyAdmin</a>
        </li>
      </ul>
      <section class="right-sidebar">
        <a class="right-sidebar__link right-sidebar__link--links" href=""></a>
        <a class="right-sidebar__link right-sidebar__link--upload" href=""></a>
      </section>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>
