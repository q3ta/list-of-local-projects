'use strict';

$(function() {
  // Seatch.
  var search = $('.js-search-site').val();
  var listOfSites = [];

  $.each($('.js-list-item'), function(i, e) {
    listOfSites.push($(e).attr('data-url'));
  });

  $('.js-search-site').keyup(function(k) {
    var word = $(this).val();
    var count = 0;
    var last = '';

    $.each(listOfSites, function(i, e) {
      if(e.toLowerCase().substr(0, word.length) != word) {
        $('.js-list-item[data-url="' + e + '"]')
          .css({'display': 'none'});
      }
      else {
        $('.js-list-item[data-url="' + e + '"]')
          .css({'display': 'block'});

        count++;
        last = e;
      }
    });

    if(k.keyCode == 13 && count == 1) {
      window.open($('.js-list-item[data-url="' + last + '"] a').attr('href'), '_blank');
    }
  });

  // Time.
  setInterval(function() {
    var now = new Date();
    if(!$('.js-time').is(':visible'))
      $('.js-time').css({'display': 'block'});

    $('.js-time-hours').text(("0" + now.getHours()).slice(-2));
    $('.js-time-minutes').text(("0" + now.getMinutes()).slice(-2));
    $('.js-time-seconds').text(("0" + now.getSeconds()).slice(-2));
  }, 1000);
});