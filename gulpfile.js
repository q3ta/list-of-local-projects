'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync');
var less = require('gulp-less');

gulp.task('server', function() {
  browserSync.init({
    'proxy': 'localhost'
  });
});

gulp.task('css', function() {
  return gulp.src('less/*.less')
    .pipe(less())
    .pipe(gulp.dest('css/'));
});

gulp.task('watch', function() {
  gulp.watch('less/*.less', ['css']);
  gulp.watch(['index.php', 'css/style.css']).on('change', browserSync.reload);
});

gulp.task('default', ['css', 'server', 'watch']);